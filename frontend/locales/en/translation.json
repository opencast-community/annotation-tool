{
  "about": {
    "close": "$t(common actions.close)"
  },
  "alert": {
    "ok": "$t(common actions.ok)",
    "error": {
      "title": "Error!"
    },
    "info": {
      "title": "Information"
    },
    "warning": {
      "title": "Warning!"
    }
  },
  "annotate on": "Annotate on",
  "annotate": {
    "categories": {
      "all": "All",
      "mine": "Mine",
      "public": "Public"
    },
    "create a label": "Create a label",
    "download categories": "Download categories",
    "edit category scaling": "Edit scaling for this category",
    "edit mode": "Edit mode",
    "export not supported": "This version of {{browser}} does not support the export function. Try it on another browser.",
    "insert": "$t(common actions.insert)",
    "items visibility": {
      "collapse all": "Collapse all",
      "expand all": "Expand all",
      "head": "Items Visibility"
    },
    "new category name": "NEW CATEGORY",
    "new label placeholder": "Label value …",
    "new": {
      "placeholder": "Write a free text annotation. $t(annotate.new.title)",
      "title": "Use \"shift + return\" keys to create a new line."
    },
    "no selected track": "No tracak selected",
    "pause while editing": "Pause video during writing",
    "select track prompt": "Select a track to start annotating",
    "upload categories": "Upload categories",
    "hide category": "Hide category",
    "show category": "Show category"
  },
  "annotation": {
    "comments": {
      "cancel": "$t(common actions.cancel)",
      "count": "1 Comment",
      "count_plural": "{{count}} Comments",
      "delete": "$t(common actions.delete)",
      "edit": {
        "button": "Edit",
        "cancel": "$t(common actions.cancel)",
        "save": "$t(common actions.save)"
      },
      "insert": "$t(common actions.insert)",
      "modified": "modified on {{date}}",
      "placeholder": "Write a comment for this annotation.",
      "title": "Comments"
    },
    "edit": {
      "delete": "Delete annotation.",
      "double click to edit": "Double click to edit",
      "edit": "Edit annotation.",
      "end time": "End time",
      "free text placeholder": "Free text …",
      "set end to playhead": {
        "long": "Set the video playhead as end point.",
        "short": "OUT"
      },
      "set start to playhead": {
        "long": "Set the video playhead as start point.",
        "short": "IN"
      },
      "start time": "Start time"
    },
    "you own": "You own this annotation"
  },
  "default track": {
    "description": "Default track for user {{nickname}}",
    "name": "Default {{nickname}}"
  },
  "delete modal": {
    "cancel": "$t(common actions.cancel)",
    "delete": "$t(common actions.delete)",
    "title": "Delete",
      "warning": "Do you really want to delete the {{type}} \"{{content}}\"?"
  },
  "description": "Video annotation tool",
  "list annotation": {
    "double click to edit": "Double click to edit"
  },
  "login": {
    "error": "Failed to log in: {{error}}"
  },
  "loop controller": {
    "next": "Move to next loop",
    "previous": "Move to previous loop",
    "seconds": "s",
    "invalid loop length": "The given value for the loop length is nto valid!"
  },
  "menu": {
    "about": "About",
    "export": "Export for statistics",
    "logout": "Logout",
    "print": "Print",
    "view": {
      "annotate": {
        "head": "Annotate",
        "structured annotations": "Structured annotations",
        "text annotations": "Text annotations"
      },
      "track management": "$t(track management)",
      "head": "View",
      "loop controller": "Loop controller",
      "views": {
        "annotate view": "Annotate view",
        "head": "Views",
        "list view": "List view"
      },
      "auto expand": "Automatically expand active annotations"
    }
  },
  "new label defaults": {
    "abbreviation": "NEW",
    "description": "New"
  },
  "print": {
    "annotations": {
      "header": "Annotations",
      "table": {
        "annotation and comments": {
          "comment by": "Comment by {{author}}",
          "free text": {
            "header": "Free annotation"
          },
          "header": "Annotation/Comments"
        },
        "codes": {
          "header": "Codes"
        },
        "timecode": {
          "header": "Timecode"
        },
        "user and track": {
          "header": "User/Track"
        }
      }
    },
    "categories": {
      "header": "Used categories",
      "table": {
        "header": "Categories",
        "scales": {
          "header": "Scales"
        }
      }
    },
    "title": "Name of the video: {{title}}"
  },
  "required fields": "Required fields",
  "scale editor": {
    "cancel": "$t(common actions.cancel)",
    "delete": "$t(common actions.delete)",
    "description": {
      "label": "Description",
      "placeholder": "Insert the category description"
    },
    "edit category scale": "Edit category scale",
    "edit scales": "Edit scales",
    "name": {
      "label": "Name",
      "placeholder": "Insert the category name"
    },
    "new scale": {
      "name": "New scale",
      "value": "New scale value"
    },
    "new scale name": "New scale",
    "new scale value": "New scale value",
    "no scale": "-- NO SCALE --",
    "save": "$t(common actions.save)",
    "values": {
      "add": "Add scale value",
      "name": {
        "header": "Name"
      },
      "value": {
        "header": "Value",
        "placeholder": "Insert a decimal number."
      }
    }
  },
  "startup": {
    "creating annotation view": "Creating annotate view.",
    "creating list view": "Creating list view.",
    "creating timeline": "Creating timeline.",
    "creating views": "Start creating views.",
    "get current user": "Get current user.",
    "get users saved locally": "Get users saved locally.",
    "initializing the player": "Initializing the player.",
    "loading": "Loading",
    "loading video": "Start loading video.",
    "ready": "Ready.",
    "starting": "Starting the tool."
  },
  "timeline": {
    "add track": {
      "add": "Add",
      "button": "Add track",
      "cancel": "$t(common actions.cancel)",
      "description": {
        "label": "Description",
        "placeholder": "Insert a description for the new track"
      },
      "header": "Add new track",
      "is public": "Public",
      "name": {
        "label": "Name",
        "placeholder": "Insert the new track name"
      }
    },
    "change visibility": {
      "to private": "Make private",
      "to public": "Make public"
    },
    "delete track": "Delete track",
    "name required": "Name is required!",
    "no description": "No description",
    "no track available": {
      "short": "No track available",
      "long": "No track corresponding with the current filter(s)."
    },
    "track management": "$t(track management)",
    "hidden items": "One hidden item",
    "hidden items_plural": "{{count}} hidden items",
    "private only": {
      "no public tracks allowed": "Public tracks have been disallowed for this video."
    },
    "reset zoom": "Reset zoom",
    "scripts not allowed": "Scripts are not allowed!",
    "track": {
      "private": "Private",
      "public": "Public",
      "you own": "You own this track"
    },
    "update track": {
      "button": "Update track",
      "cancel": "$t(common actions.cancel)",
      "description": {
        "label": "Description",
        "placeholder": "Insert a new description for the track"
      },
      "header": "Update track",
      "is public": "Public",
      "name": {
        "label": "Name",
        "placeholder": "Insert the new track name"
      },
      "update": "Update"
    }
  },
  "title": "Annotating Academic Video",
  "track management": "Track management",
  "track selection": {
    "(un-)select all": "Select/unselect all",
    "cancel": "$t(common actions.cancel)",
    "header": "$t(other public annotations)",
    "ok": "$t(common actions.ok)",
    "search": {
      "clear": "Clear",
      "hint": "Users with public annotations",
      "placeholder": "Type here to search …"
    },
    "selection header": "Select visible tracks",
    "ordering header": "Order tracks",
    "move up": "Move up",
    "move down": "Move down"
  },
  "untitled video": "Untitled video",
  "user login": {
    "email": {
      "label": "Email",
      "placeholder": "Insert your email address"
    },
    "header": "User login",
    "login": "Log in",
    "login as supervisor": "Log in as supervisor",
    "nickname": {
      "hint": "Create a new nickname for the user you are logged in with in the current context (e.g. LTI)",
      "label": "Nickname",
      "placeholder": "Insert your nickname"
    },
    "remember me": "Remember me"
  },
  "version": "Version: {{version}}",
  "annotation visibility":  {
      "all public": "All public annotations",
      "only mine": "My annotations only",
      "other public": "Other public annotations …"
  },
  "common actions": {
    "cancel": "Cancel",
    "close": "Close",
    "ok": "OK",
    "insert": "Insert",
    "save": "Save",
    "delete": "Delete"
  }
}
